# Controls
Controls per teclat:

| Tecles | Acció                |
| ------ | ------               |
|A/D     |`Moviment personatge` |
|W       |`Saltar`              |
|X       |`Baixar`              |
|Q       |`Tirar Bomba`         |
|E       |`Combo 1`             |
|R       |`Combo 2`             |
|F       |`Escut`               |

Controls per pantalla:

| Tecles | Acció                |
| ------ | ------               |
|J/L     |`Moviment personatge` |
|I       |`Saltar`              |
|K       |`Baixar`              |
|U       |`Tirar Bomba`         |
|O       |`Combo 1`             |
|P       |`Combo 2`             |
|H       |`Escut`               |

## Ampliacions

- Pool
> Les bombes de Link i les boles de foc de Mario estan gestionades per una Pool
- Animator Frame by Frame
> Moviment de les hitbox dels personatges
- Efectes físics de més complexitat
> Knockback en els personatges depenent del seu handicap
- Efectes visuals extres més enllà d’efects de partícles bàsics
> Il·luminació 2D en el segon mapa. Hi ha una llum que si l'ataques, disminueix la intensitat
- Ús d'Effectors
> Platform Effector 2D en les plataformes del primer mapa. Buoyancy Effector 2D en l'aigua del segon mapa 
- Ús de Joins
> Plataformes amb Hinge Joints en el segon mapa. Si les ataques, cauen a l'aigua
- Efectes d'audio
- Menú principal amb selecció d'escenari

