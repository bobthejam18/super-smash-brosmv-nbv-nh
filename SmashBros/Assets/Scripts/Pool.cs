﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public GameObject objPrefab;

    public int poolSize;

    private Queue<GameObject> objPool;

    private void Start()
    {
        objPool = new Queue<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject newObj = Instantiate(objPrefab);
            objPool.Enqueue(newObj);
            newObj.SetActive(false);
        }
    }

    //AGAFA UN GAMEOBJECT DE LA POOL
    public GameObject GetObjFromPool()
    {
        GameObject newObj = objPool.Dequeue();
        newObj.SetActive(true);
        return newObj;
    }

    //RETORNA UN GAMEOBJECT A LA POOL
    public void ReturnObjToPool(GameObject go)
    {
        go.SetActive(false);
        objPool.Enqueue(go);
    }

}
