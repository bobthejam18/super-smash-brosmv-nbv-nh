﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public GameObject P1live;
    public GameObject P2live;
    public GameObject enemy;
    public GameObject player;
    public GameObject[] stockPlayer;
    public GameObject[] stockEnemy;

    void Start()
    {
        player.GetComponent<Player>().updatePlayerUIEvent += updatePlayerUI;
        enemy.GetComponent<Enemy>().updateEnemyUIEvent += updateEnemyUI;

        player.GetComponent<Player>().updatePlayerUIEvent += updateStockPlayer;
        enemy.GetComponent<Enemy>().updateEnemyUIEvent += updateStockEnemy;
    }

    void updatePlayerUI(int stock)
    {
        float handicap2 = (int)System.Math.Round(((player.GetComponent<Player>().vida * 100f) - 100), 2);
        P2live.GetComponent<TMPro.TextMeshProUGUI>().text = handicap2 + "%";

        //CANVIA EL COLOR DEL TEXT DEPENENT DEL HANDICAP DEL PERSONATGE
        if (handicap2 < 20)
            P2live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 1f, 1f);
        else if (handicap2 >= 20 && handicap2 < 50)
            P2live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.8f, 0.8f);
        else if (handicap2 >= 50 && handicap2 < 75)
            P2live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.5f, 0.5f);
        else if (handicap2 >= 75 && handicap2 < 100)
            P2live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.2f, 0.2f);
        else if(handicap2 >= 100)
            P2live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.0f, 0.0f);

    }

    void updateEnemyUI(int stock)
    {
        float handicap = (int)System.Math.Round(((enemy.GetComponent<Enemy>().vida * 100f) - 100), 2);
        P1live.GetComponent<TMPro.TextMeshProUGUI>().text = handicap + "%";

        //CANVIA EL COLOR DEL TEXT DEPENENT DEL HANDICAP DEL PERSONATGE
        if (handicap < 20)
            P1live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 1f, 1f);
        else if (handicap >= 20 && handicap < 50)
            P1live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.8f, 0.8f);
        else if (handicap >= 50 && handicap < 75)
            P1live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.5f, 0.5f);
        else if (handicap >= 75 && handicap < 100)
            P1live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.2f, 0.2f);
        else if (handicap >= 100)
            P1live.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(1f, 0.0f, 0.0f);
    }

    void updateStockPlayer(int stock)
    {
        //STOCK DELS PERSONATGES
        switch (stock)
        {
            case 3:
                stockPlayer[0].SetActive(true);
                stockPlayer[1].SetActive(true);
                stockPlayer[2].SetActive(true);
                break;
            case 2:
                stockPlayer[0].SetActive(true);
                stockPlayer[1].SetActive(true);
                stockPlayer[2].SetActive(false);
                break;
            case 1:
                stockPlayer[0].SetActive(true);
                stockPlayer[1].SetActive(false);
                stockPlayer[2].SetActive(false);
                break;
            case 0:
                stockPlayer[0].SetActive(false);
                stockPlayer[1].SetActive(false);
                stockPlayer[2].SetActive(false);
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }

    void updateStockEnemy(int stock)
    {
        //STOCK DELS PERSONATGES
        switch (stock)
        {
            case 3:
                stockEnemy[0].SetActive(true);
                stockEnemy[1].SetActive(true);
                stockEnemy[2].SetActive(true);
                break;
            case 2:
                stockEnemy[0].SetActive(true);
                stockEnemy[1].SetActive(true);
                stockEnemy[2].SetActive(false);
                break;
            case 1:
                stockEnemy[0].SetActive(true);
                stockEnemy[1].SetActive(false);
                stockEnemy[2].SetActive(false);
                break;
            case 0:
                stockEnemy[0].SetActive(false);
                stockEnemy[1].SetActive(false);
                stockEnemy[2].SetActive(false);
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }
}
