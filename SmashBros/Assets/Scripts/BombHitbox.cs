﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombHitbox : MonoBehaviour
{
    public delegate void damagePlayer(float n);
    public event damagePlayer damagePlayerEvent;

    public float damage;

    //HITBOX DE LA BOMBA
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Enemy":
                collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                float knockBack = collision.gameObject.GetComponent<Enemy>().vida;
                if (this.gameObject.GetComponent<Transform>().position.x >= collision.gameObject.GetComponent<Transform>().position.x)
                {
                    collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-250f * knockBack, 250f * knockBack));
                }
                else
                {
                    collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(250f * knockBack, 250f * knockBack));
                }
                if (damagePlayerEvent != null)
                {
                    damagePlayerEvent.Invoke(damage);
                }
                break;
        }
    }
}
