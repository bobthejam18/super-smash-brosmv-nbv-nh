﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour
{
    public delegate void activateCollider();
    public event activateCollider activateColliderEvent;

    //QUAN EL PERSONATGE SURT DE LA PLATAFORMA PER ABAIX, ES TORNA A ACTIVAR EL SEU COLLIDER EN EL CAS QUE S'HAGUÉS DESACTIVAT
    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "PlataformaTrigger":
                if (activateColliderEvent != null)
                {
                    activateColliderEvent.Invoke();
                }
                break;
        }
    }
}
