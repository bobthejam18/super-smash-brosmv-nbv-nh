﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMap2 : MonoBehaviour
{
    //AQUEST SCRIPT EL QUE FA ES DESTRUIR LA PLATAFORMA DEL MAPA 2 UN COP HA CAIGUT A L'AIGUA
    public GameObject stick;

    void Start()
    {
        stick.GetComponent<Stick>().destroyPlatformEvent += destroyPlatform;
    }

    void destroyPlatform()
    {
        Invoke("InvokeDestroy", 5f);
    }

    void InvokeDestroy()
    {
        Destroy(this.gameObject);
    }


}
