﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    GameObject particles;
    GameObject hitbox;

    Animator anim;

    Pool poolManager;

    GameObject soundmanager;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        hitbox = transform.GetChild(0).gameObject;
        particles = transform.GetChild(1).gameObject;
        GameObject go = GameObject.Find("ManagerBomb");
        poolManager = go.GetComponent<Pool>();
        soundmanager = GameObject.Find("SoundManager");
    }

    //ES CRIDA QUAN L'OBJECTE ÉS ACTIVAR PER LA POOL
    public void enableBomb()
    {
       Invoke("Timer1", 1f);
       Invoke("Timer2", 3f);
       Invoke("Timer3", 4f);
    }

    //RESETEJA LES VARIABLES UN COP TORNA EL GAMEOBJECT A LA POOL
    public void disableBomb()
    {
        hitbox.SetActive(false);
        particles.SetActive(false);
        anim.SetBool("isExploding", false);
        this.GetComponent<SpriteRenderer>().enabled = true;
    }

    void Timer1()
    {
        if (anim != null)
            anim.SetBool("isExploding", true);
    }

    void Timer2()
    {
        hitbox.SetActive(true);
        particles.SetActive(true);
        this.GetComponent<SpriteRenderer>().enabled = false;
        soundmanager.GetComponent<SoundManager>().Play_SFXBomb();
    }

    void Timer3()
    {
        poolManager.ReturnObjToPool(this.gameObject); //TORNA EL GAMEOBJECT A LA POOL
        this.disableBomb();
    }

}
