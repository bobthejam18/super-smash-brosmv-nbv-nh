﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    public GameObject platform;
    public delegate void destroyPlatform();
    public event destroyPlatform destroyPlatformEvent;
    GameObject soundmanager;

    void Start()
    {
        soundmanager = GameObject.Find("SoundManager");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(collision.gameObject.tag)
        {
            //SI UN ATAC TOCA EL PAL DE LA PLATAFORMA DEL MAPA2, LA PLATAFORMA CAU
            case "Hitbox":
                platform.GetComponent<HingeJoint2D>().enabled = false;
                soundmanager.GetComponent<SoundManager>().Play_SFXDestroyPlatform();
                if (destroyPlatformEvent != null)
                {
                    destroyPlatformEvent.Invoke();
                }
                Destroy(this.gameObject);
                break;
        }
    }
}
