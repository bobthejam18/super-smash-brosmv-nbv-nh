﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    public delegate void damagePlayer(float n);
    public event damagePlayer damagePlayerEvent;

    public float damage;

    Pool poolManager;

    void Start()
    {
        GameObject go = GameObject.Find("ManagerFireBall");
        poolManager = go.GetComponent<Pool>();
    }

    public void enableFireBall()
    {
        Invoke("destroyFireBall", 4f); //SI LA FIREBALL NO TOCA AL PERSONATGE ES DESTRUEIX SOLA
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "PlayerHurtbox":
                GameObject parent = collision.gameObject.transform.parent.gameObject;
                parent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                float knockBack = parent.gameObject.GetComponent<Player>().vida;
                if (this.gameObject.GetComponent<Transform>().position.x >= parent.gameObject.GetComponent<Transform>().position.x)
                {
                    parent.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100f * knockBack, 100f * knockBack));
                }
                else
                {
                    parent.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(100f * knockBack, 100f * knockBack));
                }
                if (damagePlayerEvent != null)
                {
                    damagePlayerEvent.Invoke(damage);
                }
                poolManager.ReturnObjToPool(this.gameObject);
                break;

            case "Shield":
                poolManager.ReturnObjToPool(this.gameObject);
                break;
        }
    }

    void destroyFireBall()
    {
        poolManager.ReturnObjToPool(this.gameObject);
    }
}
